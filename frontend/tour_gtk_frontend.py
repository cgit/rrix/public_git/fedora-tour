# -*- coding: utf-8 -*-
#
# This file is a part of Fedora-tour http://fedoraproject.org/wiki/Fedora-tour
#
# Copyright (c) 2009 Ryan Rix
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

import io
import gtk

"""
This class is responsible for the creation and destruction of the main window.
"""
class Frontend:
	def __init__(self):
		self.mainWindow=self.create_win(defaultsize=[700,500])
		self.mainWindow.connect("delete_event", self.winDelete)
		self.mainWindow.connect("destroy",self.winDestroy)
		
		builder = InitialTreeView(self.mainWindow)
	
	def winDestroy(self,widget,Data=None):
		return False
		
	def winDelete(self,widget,Data=None):
		gtk.main_quit()
	
	def create_win(self,title="Fedora Tour",defaultsize=[100,100]):
		window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		window.set_title(title)
		window.set_position(gtk.WIN_POS_CENTER)
		window.set_default_size(defaultsize[0],defaultsize[1])
		return window
	
	def main(self):
		gtk.main()

class InitialTreeView:
	def __init__(self, window):
		self.draw(window)
		window.show()
	
	def draw(self, window):
		#image = gtk.Image()
		#image.set_from_file("/usr/share/pixmaps/fedora-logo-small.png")
		image2 = gtk.Image()
		image2.set_from_file("../data/tree.svg")
		#window.add(image)
		window.add(image2)
		image2.show()
		pass
	
	def erase(self):
		pass

if __name__=="__main__":
	frontend = Frontend()
	frontend.main()